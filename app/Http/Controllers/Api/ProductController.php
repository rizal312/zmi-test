<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product\MProduct;

class ProductController extends Controller
{

    // this function is the main of API
    public function index() {
        return response()->json([
            'message' => 'Welcome to Product API'
        ]);
    }

    // for fetch all products
    public function allproducts() {

        $data_products = [];
        
        try {

            $get_products = MProduct::whereNull('deleted_at')->get();

            foreach($get_products as $products) {

                array_push($data_products, [
                    'product_id'    => $products->id,
                    'product_name'  => $products->product_name,
                    'created_at'    => $products->created_at != null ? date('Y-m-d', strtotime($products->created_at)) : '-'
                ]);

            }

            return response()->json([
                'message'  => 'Success',
                'data'     => $data_products
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'message' => 'Failed',
                'data'    => null
            ], 201);

        }

    }

    // for fetch detail of product by id
    public function detailproduct(Request $request) {
        $id_product = $request->id;

        $detail_product = [];

        try {

            $get_detail_product = MProduct::whereNull('deleted_at')
                                  ->where('id', $id_product)
                                  ->with('category')
                                  ->with('variant')
                                  ->first();

            array_push($detail_product, [
                'product_id'           => $get_detail_product->id,
                'product_name'         => $get_detail_product->product_name,
                'product_variant'      => $get_detail_product->variant->variant_name,
                'product_category'     => $get_detail_product->category->category_name
            ]);

            return response()->json([
                'message'  => 'Success',
                'data'     => $detail_product
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'message'   => 'Failed',
                'data'      => null
            ], 201);

        }
    }
}
