<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product\MVariant;

class VariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = [
            'variant' => MVariant::whereNull('deleted_at')->get()
        ];

        return view('variant.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'variant_route_store' => route('variant.store')
        ];

        return view('variant.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'variant_name' => 'required'
        ]);

        try {

            MVariant::create($request->all());

            return redirect()->route('variant.index');

        } catch (\Exception $e) {

            return redirect()->route('variant.index');
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $variant_edit = MVariant::where('id', $id)->first();

        $data = [
            'variant_route_edit' => route('variant.update', $variant_edit->id),
            'variant_edit' => $variant_edit
        ];

        return view('variant.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'variant_name' => 'required'
        ]);

        try {

            $variant = MVariant::findOrFail($id);
            $variant->variant_name = $request->variant_name;

            $variant->update();

            return redirect()->route('variant.index');

        } catch(\Exception $e) {

            return redirect()->route('variant.index');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $variant = MVariant::findOrFail($id);
            $variant->deleted_at = date('Y-m-d H:i:s');

            $variant->update();

            return redirect()->route('variant.index');

        } catch(\Exception $e) {

            return redirect()->route('variant.index');

        }
    }
}
