<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Category\MProductCategory;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'category' => MProductCategory::whereNull('deleted_at')->get()
        ];

        return view('category.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'category_route_store' => route('category.store')
        ];

        return view('category.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required'
        ]);

        try {

            MProductCategory::create($request->all());

            return redirect()->route('category.index');

        } catch(\Exception $e) {

            return redirect()->route('category.index');
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $category_edit = MProductCategory::where('id', $id)->first();

        $data = [
            'category_route_edit' => route('category.update', $category_edit->id),
            'category_edit' =>  $category_edit
        ];

        return view('category.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_name' => 'required'
        ]);

        try {

            $category = MProductCategory::findOrFail($id);
            $category->category_name = $request->category_name;

            $category->update();

            return redirect()->route('category.index');

        } catch(\Exception $e) {

            return redirect()->route('category.index');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $category = MProductCategory::findOrFail($id);

        $category->deleted_at = date('Y-m-d H:i:s');

        $category->update();


        return redirect()->route('category.index');

    }
}
