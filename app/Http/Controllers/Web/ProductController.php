<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Product\MProduct;
use App\Models\Category\MProductCategory;
use App\Models\Product\MVariant;
use App\Models\Image\MProductImage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $data = [
            'product' => MProduct::whereNull('deleted_at')->with('variant')->with('category')->with('productimage')->get(),
        ];

        return view('product.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'product_route_store' => route('product.store'),
            'list_category' => MProductCategory::whereNull('deleted_at')->get(),
            'list_variant' => MVariant::whereNull('deleted_at')->get()
        ];

        return view('product.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name'   => 'required',
            'image_file'     => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        try {

            $image_file = $request->file('image_file');

            $image_file->storeAs('public/product/image/', $image_file->hashName());
            $name_product_image = 'product/image/'.$image_file->hashName();

            // collect data new product for store to table m_products
            $new_product = [
                'product_name' => $request->product_name,
                'id_category'  => $request->id_category,
                'id_variant'   => $request->id_variant
            ];

            $product = MProduct::create($new_product);

            // collect data image product and id for store to m_product_images
            $image_product = [
                'id_product' => $product->id,
                'image_file' => $name_product_image
            ];

            MProductImage::create($image_product);

            return redirect()->route('product.index');
        } catch(\Exception $e) {

            return redirect()->route('product.index');
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product_edit = MProduct::where('id', $id)->first();

        $data = [
            'product_edit' => $product_edit,
            'product_route_edit' => route('product.update', $product_edit->id),
            'list_category' => MProductCategory::all(),
            'list_variant' => MVariant::all()
        ];

        return view('product.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => 'required'
        ]);

        try {
            $product = MProduct::findOrFail($id);

            $product->product_name = $request->product_name;
            $product->id_category = $request->id_category;
            $product->id_variant = $request->id_variant;

            $product->update();

            return redirect()->route('product.index');
        } catch(\Exception $e) {

            return redirect()->route('product.index');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {

            // using soft delete for delete the record from table m_products

            $product = MProduct::findOrFail($id);

            $product->deleted_at = date('Y-m-d H:i:s');

            $product->update();

            return redirect()->route('product.index');

        } catch (\Exception $e) {

            return redirect()->route('product.index');

        }
    }
}
