<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MProductCategory extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    function products() {
        return $this->hasMany(Product\MProduct::class, 'id_category');
    }
}
