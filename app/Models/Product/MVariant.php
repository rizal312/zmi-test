<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MVariant extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    function products() {
        return $this->hasMany(Product\MProduct::class, 'id_variant');
    }
}
