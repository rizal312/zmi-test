<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Product\MVariant;
use App\Models\Category\MProductCategory;
use App\Models\Image\MProductImage;

class MProduct extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    function variant() {
        return $this->hasOne(MVariant::class, 'id', 'id_variant');
    }

    function category() {
        return $this->hasOne(MProductCategory::class, 'id', 'id_category');
    }

    function productimage() {
        return $this->hasOne(MProductImage::class, 'id_product', 'id');
    }
}
