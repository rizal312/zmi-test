<?php

namespace App\Models\Image;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MProductImage extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
}
