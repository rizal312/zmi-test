<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Web\ProductController;
use App\Http\Controllers\Web\ProductCategoryController;
use App\Http\Controllers\Web\VariantController;
use App\Http\Controllers\Web\ProductImageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// route for product 
Route::get('/', [ProductController::class, 'index'])->name('product.index');
Route::get('/product/create', [ProductController::class, 'create'])->name('product.create');
Route::post('/product/store', [ProductController::class, 'store'])->name('product.store'); 
Route::get('/product/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
Route::post('/product/update/{id}', [ProductController::class, 'update'])->name('product.update');
Route::get('/product/delete/{id}', [ProductController::class, 'destroy'])->name('product.delete');




// route for category
Route::get('/category', [ProductCategoryController::class, 'index'])->name('category.index');
Route::get('/category/create', [ProductCategoryController::class, 'create'])->name('category.create');
Route::post('/category/store', [ProductCategoryController::class, 'store'])->name('category.store'); 
Route::get('/category/edit/{id}', [ProductCategoryController::class, 'edit'])->name('category.edit');
Route::post('/category/update/{id}', [ProductCategoryController::class, 'update'])->name('category.update');
Route::get('/category/delete/{id}', [ProductCategoryController::class, 'destroy'])->name('category.delete');




// route for variant
Route::get('/variant', [VariantController::class, 'index'])->name('variant.index');
Route::get('/variant/create', [VariantController::class, 'create'])->name('variant.create');
Route::post('/variant/store', [VariantController::class, 'store'])->name('variant.store'); 
Route::get('/variant/edit/{id}', [VariantController::class, 'edit'])->name('variant.edit');
Route::post('/variant/update/{id}', [VariantController::class, 'update'])->name('variant.update');
Route::get('/variant/delete/{id}', [VariantController::class, 'destroy'])->name('variant.delete');


