<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Variants</title>
</head>
<body>
    <h3>Edit Variants</h3>

    <a href="{{ route('variant.index') }}">Back</a>

    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ $data['variant_route_edit'] }}" method="POST">
        @csrf

        <input type="text" name="variant_name" id="variant_name" value="{{ $data['variant_edit']['variant_name'] }}">
        <br>

        <button type="submit">Edit</button>
    </form>
</body>
</html>