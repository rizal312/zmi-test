<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Variants</title>
</head>
<body>
    <h3>Variants</h3>

    <a href="{{ route('variant.create') }}">Add Variant</a>
    <a href="{{ route('product.index') }}">Products</a>

    <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Variant Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>

        <tbody>
            @if(count($data['variant']) >= 1)

                @foreach($data['variant'] as $variant)

                    <tr>
                        <td align="center">{{ $variant->variant_name }}</td>
                        <td align="center">
                            <a href="{{ route('variant.edit', $variant->id) }}">Edit</a>
                        </td>
                        <td align="center">
                            <a href="{{ route('variant.delete', $variant->id) }}">Delete</a>
                        </td>
                    </tr>

                @endforeach

            @else

                <tr>
                    <td colspan="3" align="center">No Variant Here</td>
                </tr>

            @endif
        </tbody>
    </table>
</body>
</html>