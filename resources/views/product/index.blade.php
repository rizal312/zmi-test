<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product</title>
</head>
<body>

    <h3>List Products</h3>

    <a href="{{ route('product.create') }}">Add Product</a>
    <a href="{{ route('category.index') }}">Category</a>
    <a href="{{ route('variant.index') }}">Variant</a>

    <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <thead>
            <th>Image</th>
            <th>Product Name</th>
            <th>Variant</th>
            <th>Category</th>
            <th>Edit</th>
            <th>Delete</th>
        </thead>

        <tbody>
            @if(count($data['product']) >= 1)

                @foreach($data['product'] as $product)

                    <tr>
                        <td align="center"><img src="{{ URL::to("/storage/" . $product->productimage->image_file) }}" alt="Product image" width="100" height="100"></td>
                        <td align="center">{{ $product->product_name }}</td>
                        <td align="center">{{ $product->category->category_name }}</td>
                        <td align="center">{{ $product->variant->variant_name }}</td>
                        <td align="center">
                            <a href="{{ route('product.edit', $product->id) }}">Edit</a>
                        </td>
                        <td align="center">
                            <a href="{{ route('product.delete', $product->id)  }}">Delete</a>
                        </td>
                    </tr>

                @endForeach

            @else

                <tr>
                    <td colspan="5">No Products Here</td>
                </tr>
                
            @endIf
        </tbody>
    </table>
    
</body>
</html>