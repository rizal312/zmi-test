<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product</title>
</head>
<body>

    <h3>Add Product</h3>

    <a href="{{ route('product.index') }}">Back</a>


    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ $data['product_route_store'] }}" method="post" enctype="multipart/form-data">
        @csrf

        <input type="text" name="product_name" value="">
        <br>

        <input type="file" name="image_file"><br>

        <select name="id_category" id="id_category">
            @foreach($data['list_category'] as $category)
                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
            @endForeach
        </select>
        <br>

        <select name="id_variant" id="id_variant">
            @foreach($data['list_variant'] as $variant)
                <option value="{{ $variant->id }}">{{ $variant->variant_name }}</option>
            @endForeach
        </select>
        <br>

        <button type="submit">Save</button>
    </form>
    
</body>
</html>