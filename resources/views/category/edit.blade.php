<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product Categories</title>
</head>
<body>
    <h3>Edit Product Categories</h3>

    <a href="{{ route('category.index') }}">Back</a>

    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ $data['category_route_edit'] }}" method="POST">
        @csrf

        <input type="text" name="category_name" id="category_name" value="{{ $data['category_edit']['category_name'] }}">
        <br>

        <button type="submit">Edit</button>
    </form>
</body>
</html>