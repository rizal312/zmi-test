<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product Categories</title>
</head>
<body>
    <h3>Product Categories</h3>

    <a href="{{ route('category.create') }}">Add Product Category</a>
    <a href="{{ route('product.index') }}">Products</a>

    <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Category Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>

        <tbody>
            @if(count($data['category']) >= 1)

                @foreach($data['category'] as $category)

                    <tr>
                        <td align="center">{{ $category->category_name }}</td>
                        <td align="center">
                            <a href="{{ route('category.edit', $category->id) }}">Edit</a>
                        </td>
                        <td align="center">
                            <a href="{{ route('category.delete', $category->id) }}">Delete</a>
                        </td>
                    </tr>

                @endforeach

            @else

                <tr>
                    <td colspan="3" align="center">No Categories Here</td>
                </tr>

            @endif
        </tbody>
    </table>
</body>
</html>